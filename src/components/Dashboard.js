import React, { Component } from 'react';
import AllPosts from './AllPosts.js';
import Notifications from './Notifications.js';



class Dashboard extends Component {
    render() {
        return (
            <div>
                <AllPosts />
                <Notifications />
            </div>
        );
    }
}

export default Dashboard;