import React, { Component } from 'react';
import firebase from '../firebase.js';


class SignIn extends Component {
    constructor() {
        super();
        this.state = {
            loginEmail: '',
            loginPassword: ''
        }
    }
    handleChange = (e) => {
        let abcd = firebase.auth().currentUser;
       console.log(abcd)
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        firebase.auth().signInWithEmailAndPassword(this.loginEmail, this.loginPassword).then(() => {
            console.log("logged successfully!")
        })
    }

    render() {
        return (
            <React.Fragment>
                <h1>SIGN IN</h1>
                <form onSubmit={this.handleSubmit}>
                    <label htmlFor="loginEmail">Email:</label>
                    <input onChange={this.handleChange} type="text" id="loginEmail" />

                    <label htmlFor="loginPassword">Password:</label>
                    <input onChange={this.handleChange} type="password" id="loginPassword" />

                    <button type="submit">Submit</button>
                </form>
            </React.Fragment>
        )
    }
}

export default SignIn;