import React, { Component } from 'react';
import Navbar from './Navbar.js';
import Dashboard from './Dashboard.js';
import LastPosts from './LastPosts.js';
import MyPosts from './MyPosts.js';
import AddPost from './AddPost.js';
import SignUp from './SignUp.js';
import SignIn from './SignIn.js';
import SinglePostview from './SinglePostview.js';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import {updatePosts} from '../actions/actions.js';
import {connect} from 'react-redux';
import {allPosts} from '../actions/actions.js';

class App extends Component {

  componentDidMount() {
    this.props.fetchAllPosts();
  }

  render() {
    return (
      <div className="App">
      <BrowserRouter>
        <div>
        <Navbar />
        
        <Switch>
        <Route exact path="/" component={Dashboard} />
        <Route path="/lastposts" component={LastPosts} />
        <Route path="/myposts" component={MyPosts} />
        <Route path="/addpost" component={AddPost} />
        <Route path="/singlepostview/:postid" component={SinglePostview} />
        <Route path="/signup" component={SignUp} />
        <Route path="/signin" component={SignIn} />
        </Switch>
        </div>
        </BrowserRouter>
      </div>
    );
  }
}


const mapDispatchToProps = dispatch => {
  return {
      fetchAllPosts: () => {dispatch(allPosts())}
  }
}

export default connect(null, mapDispatchToProps)(App);
