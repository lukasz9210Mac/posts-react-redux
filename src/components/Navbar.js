import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import firebase from '../firebase.js';

class Navbar extends Component {

    logOut = () => {
      firebase.auth().signOut().then(() => {
        alert("WYLOGOWALES SIE")
      }).catch(err => {
        console.log(err)
      })
    }

    render() {
        return (
          <header className="header">
            <div className="wrapper">
             <nav className="mainNav">
              <NavLink to="/">Dashboard</NavLink>
              <NavLink to="/myposts" >My Posts</NavLink>
              <NavLink to="/lastposts" >Last Posts</NavLink>
              <NavLink to="/addpost">Dodaj post</NavLink>
             </nav>
             <nav>
              <button onClick={this.logOut}>Log out</button>
             </nav>
            </div>
          </header>
          
        );
      }
}

export default Navbar;