import React, { Component } from 'react';
import firebase from '../firebase.js';
import SinglePost from './SinglePost.js';
import {connect} from 'react-redux';
import moment from 'moment';
import PostDetails from './PostDetails.js';
import {Link} from 'react-router-dom' 


class LastPosts extends Component {
    render() {
        return (
            <React.Fragment>
                <h1>LastPosts COMPONENT</h1>
                {this.props.allPosts.loading ? 
                <h1>LOADING...</h1> :
                <div className="postsContainer">
                    {
                        this.props.allPosts.length ? 
                        this.props.allPosts.map(post => {
                            return (
                                <Link to={"/singlepostview/" + post.postId}  >
                                     <PostDetails  
                                        key={post.postId}
                                        title={post.title}
                                        text={post.text}
                                        date={post.date}
                                    />
                                </Link>
                               
                            )
                        }) : 
                        <h1>LOADING...</h1>
                    }
                </div> }
            </React.Fragment>
        );
    }
}



const mapStateToProps = (state) => {
    // console.log(state);
    let postsArray = [];
    let postsObj = state.posts.allPosts;
        for (let post in postsObj) {
            postsArray.push(postsObj[post])
        }
    return {
      allPosts: postsArray,
      loading: state.posts.loading
    }
}


export default connect(mapStateToProps)(LastPosts);

