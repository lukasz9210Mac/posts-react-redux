import React, {Component} from 'react'


class SinglePost extends Component {
    render() {
        return (
            <div>
                <h3>{this.props.title}</h3>
                <h6>{this.props.text}</h6>
            </div>
        )
    }
}

export default SinglePost