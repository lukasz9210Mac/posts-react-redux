import React, {Component} from 'react';
import {connect} from 'react-redux';



class SinglePostview extends Component {
    render() {
        if (!this.props.post){
            return null;
        }
        return (
            <div>
                <h2>{this.props.post.title}</h2>
                <p>{this.props.post.text}</p>
                <span>{this.props.post.date}</span>
                <span>Author: {this.props.post.userName}</span>
            </div>

        )
    }
}

const mapStateToProps = (state, ownprops) => {
    const postId = ownprops.match.params.postid
    return {
        postId,
        post: state.posts.allPosts[postId],
    }
}


export default connect(mapStateToProps)(SinglePostview);