import React, { Component } from 'react';
import firebase from '../firebase.js';
import {signUp} from '../actions/actions.js';
import {connect} from 'react-redux';

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            firstName: '',
            lastName: ''
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        const {email, password, firstName, lastName} = this.state
        this.props.signUp(email, password, firstName, lastName);
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <label htmlFor="email">Email</label>
                <input onChange={this.handleChange} type="text" id="email" name="email" />

                <label htmlFor="password">Hasło</label>
                <input onChange={this.handleChange} type="password" name="password" id="password" />

                <label htmlFor="firstName">First name:</label>
                <input onChange={this.handleChange} type="text" id="firstName" />

                <label htmlFor="lastName">Last name:</label>
                <input onChange={this.handleChange} type="text" id="lastName" />

                <button type="submit">Submit</button>
            </form>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        signUp: (email, password, firstName, lastName) => {dispatch(signUp(email, password, firstName, lastName))}
    }
}

export default connect(null, mapDispatchToProps)(SignUp);