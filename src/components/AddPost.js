import React, { Component } from 'react';
import {connect} from 'react-redux';
import {addPost} from '../actions/actions.js';

class AddPost extends Component {
    constructor() {
        super();
        this.state = {
            title: '',
            text: ''
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.props.addPost(this.state)
    }

  

    render() {
        return (
            <div>
                <h3>Add Post</h3>
                <form onSubmit={this.onSubmit}>
                    <label htmlFor="title">Post title:</label>
                    <input onChange={this.handleChange} type="text" id="title" />
                    <label htmlFor="text">Text:</label>
                    <textarea onChange={this.handleChange} id="text" />
                    <button type="submit">Submit</button>
                </form>
            </div>
            
        );
    }
}


const mapStateToProps = (state) => {
    return {
        
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addPost: (state) => {dispatch(addPost(state))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPost);