import React, { Component } from 'react';
import {connect} from 'react-redux';
import firebase from '../firebase.js';
import {fetchUserPosts} from '../actions/actions.js';
import PostDetails from './PostDetails.js';
import {Link} from 'react-router-dom' 

class MyPosts extends Component {

    componentDidMount() {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                const userId = user.uid
        this.props.fetchUserPosts(userId)
            }
        })
    }

    render() {
 
        return (
            <div>
                <h1>MyPosts COMPONENT</h1>
                {this.props.loading ? 
                <h1>LOADING...</h1> :
                <div className="postsContainer">
                   {
                       this.props.userPosts.map(post => {
                           return (
                            <Link to={"/singlepostview/" + post.postId}  >
                            <PostDetails  
                               key={post.postId}
                               title={post.title}
                               text={post.text}
                               date={post.date}
                           />
                            </Link>
                           )
                       })
                   }
                </div>}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        allPosts: state.posts.allPosts,
        userPosts: state.posts.userPosts,
        loading: state.posts.loading
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchUserPosts: (userId) => {dispatch(fetchUserPosts(userId))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyPosts);