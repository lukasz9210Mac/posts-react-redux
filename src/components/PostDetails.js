import React, {Component} from 'react';



class PostDetails extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        
        const {title, text, date, userName} = this.props
        return (
           
            <div className="entirePost">
            <h2>{title}</h2>
            <p>{text}</p>
            <span>{date}</span>
            <span>{userName}</span>
          </div>
        )
    }
}

export default PostDetails;