import firebase from '../firebase.js';



export function addPost(postData) {
    //console.log("ADD_POST action fired!");
    let ref = firebase.database().ref('posts').push()
    let key = ref.key
    let userName
    const currentUserId = firebase.auth().currentUser.uid
    firebase.database().ref('users/' + currentUserId + '/firstName').once('value', (userNamesnap) => {
        console.log("user name", userNamesnap.val())
        userName = userNamesnap.val()
    }).then(() => {
        ref.update({
            title: postData.title,
            text: postData.text,
            userId: currentUserId,
            date: new Date,
            postId: key,
            userName
        }).then((res) => {
            alert("post added!") //have to clean inputs after submit
            firebase.database().ref('users/' + currentUserId + '/posts').once('value', (snapshot) => {
                //console.log("posty uzytkownika", snapshot.val())
                if(snapshot.val() == null) { //tego ifa trzeba skrocic
                    //console.log("WARIANT 1")
                    firebase.database().ref('users/' + currentUserId + '/posts').update([key])
                } else {
                    //console.log("WARIANT 2")
                    let arr = snapshot.val()
                    let arrupdated = [...arr, key]
                    firebase.database().ref('users/' + currentUserId + '/posts').update(arrupdated)
                }
            })
            //console.log("response", res)
        })
    })
    
    return {type: "ADD_POST", payload: postData}
}


export function allPosts() {
    return (dispatch) => {
        
    dispatch(postsLoading())
    firebase.database().ref('posts/').on('value', (snapshot) => {
        console.log('1', snapshot.val());
        dispatch({type: "ALL_POSTS", postsArray: snapshot.val(), loading: false})
    })
    }
}

export function postsLoading() {
    console.log("POSTS_LOADING ACTION")
    return {
        type: "POSTS_LOADING"
    }
}

export function signUp(email, password, firstName, lastName) {
    console.log("SIGNUP ACTION FIRED")
        firebase.auth().createUserWithEmailAndPassword(email, password).then((user) => {
            console.log("user", user.user);
            // trzeba wyczyścić pola formularza po udanym submicie
            const userData = {
                email,
                firstName,
                lastName,
                uid: user.user.uid,
                date: new Date(),
                posts: []
            }
            firebase.database().ref('users/' + userData.uid).update(userData).then(()=> {
                alert("user added to databse!")
            })
            console.log("userData", userData)
        }).catch(err => {
            alert(err.message)
        })
    return {
        type: "SIGN_UP"
    }
}


export function fetchUserPosts(userId) {
    return dispatch => {
        let userPostsArr = []
    dispatch(postsLoading())
    console.log("F1")
    firebase.database().ref('users/' + userId + '/posts').once('value', (userPostssnap) => {
        console.log("userPostssnap", userPostssnap.val())
        firebase.database().ref('posts').once('value', allPostsSnap => {
            console.log("allPostsSnap", allPostsSnap.val())
            const allPostsObj = allPostsSnap.val()
            const userPostsArrIds = userPostssnap.val()
            for ( let post in allPostsObj) {
                userPostsArrIds.map((pi) => {
                  if (pi === post) {
                    userPostsArr.push(allPostsObj[post])
                  }
                })
            }
        }).then(() => {
            console.log("F2", userPostsArr)
            dispatch({type: "FETCH_USER_POSTS", userPostsArr})
        })
        
    })
    
    
    }
}