import * as firebase from 'firebase';

var config = {
    apiKey: "AIzaSyA6_z-6ziLCcj2e_ZgCsIr5wFivWZ4IJ9c",
    authDomain: "posts-react-redux.firebaseapp.com",
    databaseURL: "https://posts-react-redux.firebaseio.com",
    projectId: "posts-react-redux",
    storageBucket: "",
    messagingSenderId: "732008889145"
  };
  firebase.initializeApp(config);

export default firebase;