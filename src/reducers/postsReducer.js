const initialState = {
    loading: false,
    allPosts: [],
    userPosts: []
}

const postsReducer = (state = initialState, action) => {
    switch(action.type) {
        case "ADD_POST" :
        console.log("UDALO SIE!!!!!", action.payload)
        return state
        break;
        case "ALL_POSTS" :
        console.log("action payload all posts", action.postsArray)
        return {
            ...state,
            loading: false,
            allPosts: action.postsArray
        }
        break;
        case "POSTS_LOADING" :
        return {
            ...state,
            loading: true
        }
        break;
        case "FETCH_USER_POSTS" :
        console.log("F3", action.userPostsArr)
        return {
            ...state,
            userPosts: action.userPostsArr,
            loading: false
        }
        break;
        default:
        return state
    }
    return state
}

export default postsReducer;